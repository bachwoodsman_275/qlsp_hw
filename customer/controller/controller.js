function renderListSP(listSP) {
  // render
  var contentHTML = "";
  listSP.forEach(function (sp) {
    var content = ` 
  <div class="btn btn-secondary shadow rounded-lg" onclick ="xemThem('${sp.id}')">
    <div class="p-3 w-75 text-wrap container">
          <div>
            <img src="${sp.img}" alt="${sp.name}" class="w-50 ">
          </div>
        
      <div class="p-md-2 text-left container-fluid">

        <div class="mb-2">
        <div><span class="font-weight-bold">Name</span>: ${sp.name}</div>
        <div><span class="font-weight-bold">Price</span>: ${sp.price}</div>
        <div><span class="font-weight-bold">Desc</span>: ${sp.desc}</div>
        </div>
      
      </div>
        
    </div>
  </div>
    `;
    contentHTML += content;
  });

  document.getElementById("danhSachSP").innerHTML = contentHTML;
}

function showThongTinXemThem(sp) {
  batLoading();
  var contentHTML = `
  <div class="d-flex rounded">

    <div class="hinhAnh">
      <img src="${sp.img}" alt="${sp.name}" class="w-100 ">
    </div>
  <div class="thongTin">
      <div><span class="font-weight-bold">Name</span>: ${sp.name}</div>
      <div><span class="font-weight-bold">Price</span>: ${sp.price}</div>
      <div><span class="font-weight-bold">Screen</span>: ${sp.screen}</div>
      <div><span class="font-weight-bold">Camera Sau</span>: ${sp.backCamera}</div>
      <div><span class="font-weight-bold">Camera Trước</span>: ${sp.frontCamera}</div>
      <div><span class="font-weight-bold">Desc</span>: ${sp.desc}</div>
  </div>
  </div>

  <button class="btn btn-info mt-3 ml-3" onclick="themSanPhamVaoGio(${sp.id})">
  <i class="fa fa-shopping-cart mr-2"></i>
  Thêm vào giỏ hàng
  </button>

    `;

  document.getElementById("thongTinSP").innerHTML = contentHTML;
  tatLoading();
}

function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}
function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}

// cart
function renderSpInCart(listProInCart) {
  var contentHTML = "";
  listProInCart.forEach(function (sanPham) {
    var content = `
    <tr class="cart-row">
            <div class="cart-item">
              <td scope="col" class="col-md-2 col-lg-2">${sanPham.id}</td>
              <td scope="col" class="col-md-2 col-lg-2">
              <div>
              <img src="${sanPham.img}" alt="${sanPham.name}" class="w-100" style="border-radius: 5px"/>
              </div>
              </td>
              <td scope="col" class="cart-name col-md-2 col-lg-2">${sanPham.name}</td>
              <td scope="col" class="cart-price col-md-2 col-lg-2">${sanPham.price} nghìn</td>
              <td>
                <input
                  class="cart-quantity-input w-50"
                  type="number"
                  value="${sanPham.quantity}"
                  style="border-radius: 5px"
                  onclick="loadQuantityCart()"
                />
              </td>
              <td scope="col" class="col-md-2 col-lg-2">
                <button class="btn btn-danger" onclick="xoaSPinCart(${sanPham.id})">
                  Xóa
                </button>
              </td>
            </div>
          </tr>
    `;
    contentHTML += content;
  });
  document.getElementById("dsspCart").innerHTML = contentHTML;
}
