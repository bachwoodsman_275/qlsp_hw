var isSanPhamUpdate = null;
var listProInCart = [];

// var recieveDatajson = JSON.parse(localStorage.getItem("DSSP"));
// console.log(recieveDatajson);
tatLoading();
closeCart();
fetchSPList();
loadCart();
updateTotal();

function loadCart() {
  const dataLocal = localStorage.getItem("DSSP");
  listProInCart = dataLocal ? JSON.parse(dataLocal) : [];
  renderSpInCart(listProInCart);
  loadQuantityCart();
}

function fetchSPList() {
  batLoading();
  spServ
    .getList()
    .then(function (res) {
      var listSP = res.data;

      // render
      renderListSP(listSP);

      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
function xemThem(id) {
  $("#myModalXemThem").modal("show");
  isSanPhamUpdate = id;
  spServ
    .getById(isSanPhamUpdate)
    .then(function (res) {
      showThongTinXemThem(res.data);
    })
    .catch(function (err) {
      tatLoading();
    });
}

function luaChonSP(loaiSanPham) {
  batLoading();
  spServ
    .getList()
    .then(function (res) {
      var data = res.data;
      var selectedProduct = data.filter(function (product) {
        return product.type === loaiSanPham;
      });
      renderListSP(selectedProduct);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
/**
 * cart
khi người dùng bấm thêm sản phẩm đó vào giỏ hàng, thì giỏ hàng hiện ra 1 san phẩm. 
tính năng 1: khi người dùng nhập thêm sp, hiện số lượng sp có trong giỏ trên thẻ icon
tính năng 2: khi người dùng nhập thêm sp, tắt modal và scroll lên đầu trang

 * 
 */

function loadQuantityCart() {
  const dataCartLocal = localStorage.getItem("DSSP");
  const lengthCart = dataCartLocal ? JSON.parse(dataCartLocal).length : 0;
  let quantityInCart = getQuantityInCart();
  document.getElementById("quantity").innerText = quantityInCart;
}

function themSanPhamVaoGio(idSP) {
  isSanPhamUpdate = idSP;
  spServ
    .getById(isSanPhamUpdate)
    .then(function (res) {
      // Nếu dataLocal khác null hoặc undefined thì sẽ chuyển dữ liệu localstorage thành đối tượng js và gán cho listProInCart nếu không thì listProInCart được gán cho  1 mảng rỗng
      const dataLocal = localStorage.getItem("DSSP");
      listProInCart = dataLocal ? JSON.parse(dataLocal) : [];
      // Kiểm tra trong cart tồn tại sản phẩm hay chưa
      existCart = listProInCart.find((item) => item.id == idSP);
      // Nếu tồn tại chỉ tăng số lượng, ngược lại thì thêm sản phẩm vào giỏ
      if (existCart) {
        existCart.quantity += 1;
        loadQuantityCart();
        // Cập nhật số lượng trên icon giỏ
      } else {
        const newCart = {
          ...res.data,
          quantity: 1,
        };
        listProInCart.push(newCart);
      }
      // Cập nhật giỏ hàng trong localStorage
      var updateLocal = JSON.stringify(listProInCart);
      localStorage.setItem("DSSP", updateLocal);

      $("#myModalXemThem").modal("hide");
      openCart();

      renderSpInCart(listProInCart);
      loadQuantityCart();
      // update price có trong giỏ
      updateTotal();
    })
    .catch(function (err) {
      tatLoading();
    });
  // chuyển JSON từ chuỗi thành dữ liệu render
}
// nếu trùng id thì số lượng cộng lên 1

// open shopping cart
function openCart() {
  let cart = document.querySelector(".cart");
  cart.style.display = "block";
}
// close shopping cart
function closeCart() {
  let cart = document.querySelector(".cart");
  cart.style.display = "none";
}
// xóa sản phẩm cart
function xoaSPinCart(index) {
  // lấy dữ liệu localStorage
  let listProInCart = getListFromLocalStorage();

  // tìm sản phẩm trong giỏ có id trùng với id sản phẩm người dùng chọn
  let selectedPro = listProInCart.findIndex(function (item) {
    return item.id == index;
  });

  // xóa trong localStorage
  // chuyển obj thành mảng
  if (selectedPro !== -1) {
    listProInCart.splice(selectedPro, 1);
  }
  localStorage.setItem("DSSP", JSON.stringify(listProInCart));
  loadQuantityCart();
  renderSpInCart(listProInCart);
  updateTotal();
}

function getQuantityInCart() {
  let listPro = getListFromLocalStorage();
  var totalQuantity = 0;
  listPro.find((item) => {
    // let itemNumber = parseFloat(item.quantity);
    itemNumber = item.quantity;
    totalQuantity += itemNumber;
  });
  return totalQuantity;
}
function getListFromLocalStorage() {
  let data = localStorage.getItem("DSSP");
  let listPro = data ? JSON.parse(data) : [];
  return listPro;
}
function getPriceInCart() {
  let totalPrice = 0;
  let listPro = getListFromLocalStorage();
  listPro.find((item) => {
    let price = item.price;
    let quantity = item.quantity;
    let totalPriceEachProduct = price * quantity;
    totalPrice += totalPriceEachProduct;
  });
  return totalPrice;
}
// Cập nhật tổng tiền trong giỏ
// Tải lại số lượng sản phẩm có trong giỏ, cộng các sản phẩm * quantity
function updateTotal() {
  // Lấy được giá trị price trong object
  let totalPrice = getPriceInCart();
  localStorage.setItem("Total", JSON.stringify(totalPrice));
  let dataTotal = localStorage.getItem("Total")
    ? JSON.parse(localStorage.getItem("Total"))
    : 0;
  console.log("dataTotal :", dataTotal);
  let toTal = `<h3>$${dataTotal}</h3> `;
  // DOM tới thẻ html
  document.querySelector(".cart-total-price").innerHTML = toTal;
}
// ngày mai làm chức năng tiền và số lượng khi thay đổi number input
