var BASE_URL = "https://647f22aac246f166da9024db.mockapi.io/product";
var CART_URL = "https://647f22aac246f166da9024db.mockapi.io/productincart";
var spServ = {
  getList: function () {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  getById: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  create: function (product) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
  },
  update: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
    });
  },
};
var spCartServ = {
  getList: function () {
    return axios({
      url: CART_URL,
      method: "GET",
    });
  },
  create: function (id, product) {
    return axios({
      url: CART_URL,
      method: "POST",
      data: product,
    });
  },
};
