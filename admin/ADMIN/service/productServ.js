var BASE_URL = "https://647f22aac246f166da9024db.mockapi.io/product";
var CART_URL = "https://647f22aac246f166da9024db.mockapi.io/productincart";
var productServ = {
  getList: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  delete: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  create: (product) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
  },
  getById: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  update: function (id, product) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: product,
    });
  },
};
